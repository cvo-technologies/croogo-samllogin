# SamlLogin plugin

[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.txt)
[![Build Status](https://img.shields.io/travis/CVO-Technologies/croogo-samllogin/master.svg?style=flat-square)](https://travis-ci.org/CVO-Technologies/croogo-samllogin)
[![Coverage Status](https://img.shields.io/codecov/c/github/cvo-technologies/croogo-samllogin.svg?style=flat-square)](https://codecov.io/github/cvo-technologies/croogo-samllogin)
[![Total Downloads](https://img.shields.io/packagist/dt/cvo-technologies/croogo-samllogin.svg?style=flat-square)](https://packagist.org/packages/cvo-technologies/croogo-samllogin)
[![Latest Stable Version](https://img.shields.io/packagist/v/cvo-technologies/croogo-samllogin.svg?style=flat-square&label=stable)](https://packagist.org/packages/cvo-technologies/croogo-samllogin)

## Installation

### Using Composer
```
composer require cvo-technologies/croogo-samllogin
```

Ensure `require` is present in `composer.json`:
```json
{
    "require": {
        "cvo-technologies/croogo-samllogin": "~1.0"
    }
}
```

### Load the plugin

```php
Plugin::load('CvoTechnologies/SamlLogin');
```

## Usage
