<?php

use Croogo\Core\Croogo;

Croogo::hookComponent('*', 'CvoTechnologies/SimpleSaml.SimpleSaml');
Croogo::hookBehavior('Croogo/Users.Users', 'CvoTechnologies/SimpleSaml.SimpleSaml', [
    'attributes' => [
        'username' => 'username',
        'email' => 'email',
        'name' => 'name',
    ]
]);

Croogo::hookComponent('*', [
    'CvoTechnologies/SamlLogin.SamlAuth' => [
        'priority' => 5
    ]
]);
