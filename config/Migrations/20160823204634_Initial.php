<?php

use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    public function up()
    {
        $this->table('saml_authsources')
            ->addColumn('alias', 'string', [
                'default' => 'default-sp',
                'limit' => 32,
                'null' => false,
            ])
            ->addColumn('entity_id', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => true,
            ])
            ->addColumn('type', 'string', [
                'default' => 'saml:SP',
                'limit' => 32,
                'null' => false,
            ])
            ->addColumn('certificate', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('private_key', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('name_id_policy', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => true,
            ])
            ->addIndex(
                [
                    'alias',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('saml_hosted_identity_providers')
            ->addColumn('entity_id', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('alias', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => false,
            ])
            ->addColumn('host', 'string', [
                'default' => '__DEFAULT__',
                'limit' => 32,
                'null' => false,
            ])
            ->addColumn('certificate', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => true,
            ])
            ->addColumn('private_key', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => true,
            ])
            ->addColumn('authsource_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('name_id_format', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => true,
            ])
            ->addColumn('attribute_user_id', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => false,
            ])
            ->addColumn('attribute_email', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => true,
            ])
            ->addIndex(
                [
                    'entity_id',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('saml_identity_provider_artifact_resolution_services')
            ->addColumn('saml_identity_provider_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('binding', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('location', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('service_index', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('is_default', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'saml_identity_provider_id',
                ]
            )
            ->create();

        $this->table('saml_identity_provider_logout_services')
            ->addColumn('saml_identity_provider_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('binding', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('location', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('response_location', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => true,
            ])
            ->addIndex(
                [
                    'saml_identity_provider_id',
                ]
            )
            ->create();

        $this->table('saml_identity_provider_manage_name_id_services')
            ->addColumn('saml_identity_provider_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('binding', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('location', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('response_location', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => true,
            ])
            ->addIndex(
                [
                    'saml_identity_provider_id',
                ]
            )
            ->create();

        $this->table('saml_identity_provider_name_id_mapping_services')
            ->addColumn('saml_identity_provider_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('binding', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('location', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addIndex(
                [
                    'saml_identity_provider_id',
                ]
            )
            ->create();

        $this->table('saml_identity_provider_sign_on_services')
            ->addColumn('saml_identity_provider_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('binding', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('location', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addIndex(
                [
                    'saml_identity_provider_id',
                ]
            )
            ->create();

        $this->table('saml_identity_providers')
            ->addColumn('alias', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => false,
            ])
            ->addColumn('entity_id', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('signing_certificate', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('login_field', 'string', [
                'default' => 'username',
                'limit' => 32,
                'null' => false,
            ])
            ->addColumn('login_attribute', 'string', [
                'default' => 'nameId',
                'limit' => 32,
                'null' => false,
            ])
            ->addColumn('first_name_attribute', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => true,
            ])
            ->addColumn('last_name_attribute', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => true,
            ])
            ->addColumn('full_name_attribute', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => true,
            ])
            ->addColumn('email_attribute', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => true,
            ])
            ->addColumn('website_attribute', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => true,
            ])
            ->addColumn('create_user', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('update_user', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('metadata', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'alias',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'entity_id',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('saml_service_provider_assertion_consumer_services')
            ->addColumn('saml_service_provider_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('binding', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('location', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addIndex(
                [
                    'saml_service_provider_id',
                ]
            )
            ->create();

        $this->table('saml_service_provider_logout_services')
            ->addColumn('saml_service_provider_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('binding', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('location', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('response_location', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => true,
            ])
            ->addIndex(
                [
                    'saml_service_provider_id',
                ]
            )
            ->create();

        $this->table('saml_service_providers')
            ->addColumn('alias', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 32,
                'null' => false,
            ])
            ->addColumn('description', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('entity_id', 'string', [
                'default' => null,
                'limit' => 182,
                'null' => false,
            ])
            ->addColumn('metadata', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'entity_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'alias',
                ]
            )
            ->create();
    }

    public function down()
    {
        $this->dropTable('saml_authsources');
        $this->dropTable('saml_hosted_identity_providers');
        $this->dropTable('saml_identity_provider_artifact_resolution_services');
        $this->dropTable('saml_identity_provider_logout_services');
        $this->dropTable('saml_identity_provider_manage_name_id_services');
        $this->dropTable('saml_identity_provider_name_id_mapping_services');
        $this->dropTable('saml_identity_provider_sign_on_services');
        $this->dropTable('saml_identity_providers');
        $this->dropTable('saml_service_provider_assertion_consumer_services');
        $this->dropTable('saml_service_provider_logout_services');
        $this->dropTable('saml_service_providers');
    }
}
