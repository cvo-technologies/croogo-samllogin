<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::plugin(
    'CvoTechnologies/SamlLogin',
    ['path' => '/cvo-technologies/saml-login'],
    function (RouteBuilder $routes) {
        $routes->fallbacks('DashedRoute');
    }
);

Router::scope('/saml', ['authsource' => 'default-sp'], function (RouteBuilder $routeBuilder) {
    $routeBuilder->connect('/login/:idp', [], ['_name' => 'saml_login', 'routeClass' => 'CvoTechnologies/SimpleSaml.Login']);
    $routeBuilder->connect('/logout/:idp', [], ['_name' => 'saml_logout', 'routeClass' => 'CvoTechnologies/SimpleSaml.Logout']);
});

Router::connect('/saml/handle', ['plugin' => 'CvoTechnologies/SamlLogin', 'controller' => 'Saml', 'action' => 'handle'], ['_name' => 'simplesaml_handle']);

Router::prefix('admin', function (RouteBuilder $routeBuilder) {
   $routeBuilder->plugin('CvoTechnologies/SamlLogin',     function (RouteBuilder $routes) {
       $routes->fallbacks('DashedRoute');
   });
});
