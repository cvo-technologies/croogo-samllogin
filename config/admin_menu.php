<?php

namespace Croogo\Users\Config;

use Croogo\Core\Nav;

Nav::add('sidebar', 'users.children.saml', [
    'title' => __d('cvo_technologies/saml_login', 'SAML'),
    'url' => [
        'prefix' => 'admin',
        'plugin' => 'CvoTechnologies/SamlLogin',
        'controller' => 'ServiceProviders',
        'action' => 'info',
    ],
    'weight' => 80,
]);

Nav::add('sidebar', 'settings.children.saml', [
    'title' => __d('cvo_technologies/saml_login', 'SAML'),
    'url' => [
        'prefix' => 'admin',
        'plugin' => 'CvoTechnologies/SamlLogin',
        'controller' => 'Authsources',
        'action' => 'index',
    ],
    'children' => [
        'saml_authsources' => [
            'title' => __d('cvo_technologies/saml_login', 'Authsources'),
            'url' => [
                'prefix' => 'admin',
                'plugin' => 'CvoTechnologies/SamlLogin',
                'controller' => 'Authsources',
                'action' => 'index',
            ],
        ],
        'saml_sp' => [
            'title' => __d('cvo_technologies/saml_login', 'Service Providers'),
            'url' => [
                'prefix' => 'admin',
                'plugin' => 'CvoTechnologies/SamlLogin',
                'controller' => 'ServiceProviders',
                'action' => 'index',
            ],
        ],
        'saml_idp' => [
            'title' => __d('cvo_technologies/saml_login', 'Identity Providers'),
            'url' => [
                'prefix' => 'admin',
                'plugin' => 'CvoTechnologies/SamlLogin',
                'controller' => 'IdentityProviders',
                'action' => 'index',
            ],
        ],
        'saml_hosted_idp' => [
            'title' => __d('cvo_technologies/saml_login', 'Hosted Identity Providers'),
            'url' => [
                'prefix' => 'admin',
                'plugin' => 'CvoTechnologies/SamlLogin',
                'controller' => 'HostedIdentityProviders',
                'action' => 'index',
            ],
        ]
    ],
    'weight' => 50,
]);
