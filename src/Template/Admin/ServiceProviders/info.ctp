<h1>SAML Configuration</h1>
<div class="row">
    <div class="col-lg-8">
        <h2>Service provider</h2>
        <ul>
            <li><strong>Entity ID</strong>: <?= h($settings['sp']['entityId']); ?></li>
        </ul>

        <div class="btn-group">
            <?= $this->Html->link(__d('cvo_technologies/saml_login', 'Download metadata'), ['_name' => 'saml_metadata'], ['class' => 'btn btn-success']); ?>
        </div>

        <h2>Contacts</h2>
        <div class="row">
            <div class="col-lg-6">
                <h3>Technical contact</h3>
                <ul>
                    <li><strong>Name</strong>: <?= h($settings['contactPerson']['technical']['givenName']); ?></li>
                    <li><strong>Email</strong>: <?= h($settings['contactPerson']['technical']['emailAddress']); ?></li>
                </ul>
            </div>
            <div class="col-lg-6">
                <h3>Support contact</h3>
                <ul>
                    <li><strong>Name</strong>: <?= h($settings['contactPerson']['support']['givenName']); ?></li>
                    <li><strong>Email</strong>: <?= h($settings['contactPerson']['support']['emailAddress']); ?></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2>Organization</h2>
                <ul>
                    <li><strong>Name</strong>: <?= h($settings['organization']['en-US']['name']); ?></li>
                    <li><strong>Display name</strong>: <?= h($settings['organization']['en-US']['displayname']); ?></li>
                    <li><strong>Website</strong>: <?= h($settings['organization']['en-US']['url']); ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <h2>Certificate</h2>
        <ul>
            <li><strong>Fingerprint</strong>: <?= h($certificateFingerprint); ?></li>
            <li>
                <strong>Private key check</strong>:
                <?php if ($privateKeyMatches): ?>
                    Matches
                <?php else: ?>
                    Does not match
                <?php endif; ?>
            </li>
        </ul>
        <pre>
        <?= h(\Cake\Error\Debugger::exportVar($certificateInformation)); ?>
        </pre>

    </div>
</div>
