<?php

$this->extend('Croogo/Core./Common/admin_edit');
$this->Html->script(array('Croogo/Nodes.admin'), ['block' => true]);

$this->Html
    ->addCrumb(__d('cvo_technologies/saml_login', 'SAML'))
    ->addCrumb(__d('cvo_technologies/saml_login', 'Service providers'), ['action' => 'index']);

if ($this->request->params['action'] == 'add') {
    $this->assign('title', __d('cvo_technologies/saml_login', 'Create service provider'));

    $this->Html->addCrumb(__d('cvo_technologies/saml_login', 'Create'), ['action' => 'add']);
}

if ($this->request->params['action'] == 'edit') {
    $this->assign('title', __d('cvo_technologies/saml_login', 'Edit {0}', $serviceProvider->name));

    $this->Html->addCrumb($serviceProvider->name);
}

$this->append('form-start', $this->Form->create($serviceProvider, [
    'class' => 'protected-form',
]));

$this->start('tab-heading');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Service provider'), '#node-main');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Assertion consumer services'), '#sp-acs');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Logout services'), '#sp-slo');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Attributes'), '#sp-attributes');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Security'), '#sp-security');
$this->end();

$this->start('tab-content');
echo $this->Html->tabStart('node-main');
echo $this->Form->input('name', [
    'label' => __d('cvo_technologies/saml_login', 'Name'),
    'data-slug' => '#alias',
    'data-slug-editable' => true,
    'data-slug-edit-class' => 'btn btn-secondary btn-sm',
]);
echo $this->Form->input('alias', [
    'class' => 'slug',
    'label' => __d('cvo_technologies/saml_login', 'Alias'),
]);
echo $this->Form->input('description', [
    'label' => __d('cvo_technologies/saml_login', 'Description'),
]);
echo $this->Form->input('entity_id', [
    'label' => __d('cvo_technologies/saml_login', 'Entity ID'),
    'type' => 'text',
    'disabled' => $serviceProvider->isDefinedInMetadata('entity_id')
]);
echo $this->Form->input('metadata', [
    'label' => __d('cvo_technologies/saml_login', 'Metadata'),
    'type' => 'textarea'
]);
echo $this->Html->tabEnd();
echo $this->Html->tabStart('sp-acs');
echo $this->Form->input('acl_url', [
    'label' => __d('cvo_technologies/saml_login', 'ACS URL'),
    'disabled' => $serviceProvider->isDefinedInMetadata('sso_url')
]);
echo $this->Form->input('sso_binding', [
    'label' => __d('cvo_technologies/saml_login', 'Binding'),
    'options' => [
        'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect' => 'HTTP-Redirect',
        'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST' => 'HTTP-POST',
        'urn:oasis:names:tc:SAML:2.0:bindings:SOAP' => 'SOAP',
    ],
    'disabled' => $serviceProvider->isDefinedInMetadata('sso_binding')
]);
echo $this->Html->tabEnd();
echo $this->Html->tabStart('sp-slo');
echo $this->Form->input('slo_url', [
    'label' => __d('cvo_technologies/saml_login', 'SLO URL'),
    'disabled' => $serviceProvider->isDefinedInMetadata('slo_url')
]);
echo $this->Form->input('slo_binding', [
    'label' => __d('cvo_technologies/saml_login', 'Binding'),
    'options' => [
        'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect' => 'HTTP-Redirect',
        'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST' => 'HTTP-POST',
        'urn:oasis:names:tc:SAML:2.0:bindings:SOAP' => 'SOAP',
    ],
    'disabled' => $serviceProvider->isDefinedInMetadata('slo_binding')
]);
echo $this->Html->tabEnd('sp-logout');
echo $this->Html->tabStart('sp-attributes');
echo $this->Form->input('login_field', [
    'label' => __d('cvo_technologies/saml_login', 'Login field'),
    'options' => [
        'username' => __d('cvo_technologies/saml_login', 'Username'),
        'email' => __d('cvo_technologies/saml_login', 'Email'),
    ],
]);
echo $this->Form->input('login_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'Login attribute'),
]);
echo $this->Form->input('first_name_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'First name attribute'),
]);
echo $this->Form->input('last_name_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'Last name attribute'),
]);
echo $this->Form->input('full_name_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'Full name attribute'),
]);
echo $this->Form->input('email_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'Email attribute'),
]);
echo $this->Form->input('website_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'Website attribute'),
]);
echo $this->Html->tabEnd('sp-attributes');
echo $this->Html->tabStart('sp-security');
echo $this->Form->input('signing_cert', [
    'label' => __d('cvo_technologies/saml_login', 'Signing cert'),
    'disabled' => $serviceProvider->isDefinedInMetadata('signing_cert')
]);
echo $this->Html->tabEnd('sp-security');
$this->end();

$this->start('panels');
echo $this->Html->beginBox(__d('cvo_technologies/saml_login', 'Publishing'));
echo $this->element('Croogo/Core.admin/buttons', ['type' => $serviceProvider->name]);
echo $this->Html->endBox();
$this->end();
