<?php

use Cake\Utility\Hash;
use Croogo\Core\Status;

$this->extend('Croogo/Core./Common/admin_index');

$this->Html
    ->addCrumb(__d('cvo_technologies/saml_login', 'SAML'))
    ->addCrumb(__d('cvo_technologies/saml_login', 'Service providers', ['action' => 'index']));

