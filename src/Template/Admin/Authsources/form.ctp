<?php

$this->extend('Croogo/Core./Common/admin_edit');
$this->Html->script(array('Croogo/Nodes.admin'), ['block' => true]);

$this->Html
    ->addCrumb(__d('cvo_technologies/saml_login', 'SAML'))
    ->addCrumb(__d('cvo_technologies/saml_login', 'Authsources'), ['action' => 'index']);

if ($this->request->params['action'] == 'add') {
    $this->assign('title', __d('cvo_technologies/saml_login', 'Create authsource'));

    $this->Html->addCrumb(__d('cvo_technologies/saml_login', 'Create'), ['action' => 'add']);
}

if ($this->request->params['action'] == 'edit') {
    $this->assign('title', __d('cvo_technologies/saml_login', 'Edit {0}', $authsource->alias));

    $this->Html->addCrumb($authsource->alias);
}

$this->append('form-start', $this->Form->create($authsource, [
    'class' => 'protected-form',
]));

$this->start('tab-heading');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Authsource'), '#authsource-main');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Attributes'), '#authsource-attributes');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Security'), '#authsource-security');
$this->end();

$this->start('tab-content');
echo $this->Html->tabStart('authsource-main');
echo $this->Form->input('alias', [
    'class' => 'slug',
    'label' => __d('cvo_technologies/saml_login', 'Alias'),
]);
echo $this->Form->input('entity_id', [
    'label' => __d('cvo_technologies/saml_login', 'Entity ID'),
    'type' => 'text',
]);
echo $this->Form->input('type', [
    'label' => __d('cvo_technologies/saml_login', 'Type'),
]);
echo $this->Html->tabEnd();
echo $this->Html->tabStart('authsource-attributes');
echo $this->Form->input('name_id_policy', [
    'label' => __d('cvo_technologies/saml_login', 'NameID policy'),
    'empty' => __d('cvo_technologies/saml_login', 'None specified'),
    'options' => [
        'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified' => __d('cvo_technologies/saml_login', 'Unspecified'),
        'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress' => __d('cvo_technologies/saml_login', 'Email address'),
        'urn:oasis:names:tc:SAML:2.0:nameid-format:kerberos' => __d('cvo_technologies/saml_login', 'Kerberos'),
    ]
]);
echo $this->Html->tabEnd('authsource-attributes');
echo $this->Html->tabStart('authsource-security');
echo $this->Form->input('certificate', [
    'label' => __d('cvo_technologies/saml_login', 'Certificate path'),
]);
echo $this->Form->input('private_key', [
    'label' => __d('cvo_technologies/saml_login', 'Private key path'),
]);
echo $this->Html->tabEnd('authsource-security');
$this->end();

$this->start('panels');
echo $this->Html->beginBox(__d('cvo_technologies/saml_login', 'Publishing'));
echo $this->element('Croogo/Core.admin/buttons', ['type' => $authsource->alias]);
echo $this->Html->endBox();
$this->end();
