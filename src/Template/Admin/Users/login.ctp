<div class="card">
    <div class="card-block">
        <div class="btn-group">
            <?php foreach ($identityProviders as $identityProvider): ?>
                <?= $this->Html->link(__d('cvo_technologies/saml_login', 'Login with {0}', $identityProvider->name), [
                    '_name' => 'saml_login',
                    'idp' => $identityProvider->alias,
                    'return' => $returnUrl
                ], [
                    'class' => 'btn btn-success'
                ]); ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>
