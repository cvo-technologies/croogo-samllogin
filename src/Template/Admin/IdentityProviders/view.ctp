<?php

$this->extend('Croogo/Core./Common/admin_view');

$this->Html
    ->addCrumb(__d('cvo_technologies/saml_login', 'SAML'))
    ->addCrumb(__d('cvo_technologies/saml_login', 'Identity providers'), ['action' => 'index']);

$this->start('tab-heading');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Identity provider'), '#idp-main');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Metadata'), '#idp-metadata');
$this->end();

$this->start('tab-content');
echo $this->Html->tabStart('idp-main');
?>
<ul>
    <li><strong>Name</strong>: <?= h($identityProvider->name); ?></li>
    <li><strong>Login field</strong>: <?= h($identityProvider->login_field); ?></li>
</ul>
<?php
echo $this->Html->tabEnd('idp-main');
echo $this->Html->tabStart('idp-metadata');
?>
    <pre>
        <?= h($identityProvider->metadata); ?>
    </pre>
<?php
echo $this->Html->tabEnd('idp-metadata');
$this->end();
