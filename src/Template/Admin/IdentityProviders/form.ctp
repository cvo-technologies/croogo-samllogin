<?php

$this->extend('Croogo/Core./Common/admin_edit');
$this->Html->script(array('Croogo/Nodes.admin'), ['block' => true]);

$this->Html
    ->addCrumb(__d('cvo_technologies/saml_login', 'SAML'))
    ->addCrumb(__d('cvo_technologies/saml_login', 'Identity providers'), ['action' => 'index']);

if ($this->request->params['action'] == 'add') {
    $this->assign('title', __d('cvo_technologies/saml_login', 'Create identity provider'));

    $this->Html->addCrumb(__d('cvo_technologies/saml_login', 'Create'), ['action' => 'add']);
}

if ($this->request->params['action'] == 'edit') {
    $this->assign('title', __d('cvo_technologies/saml_login', 'Edit {0}', $identityProvider->name));

    $this->Html->addCrumb($identityProvider->name);
}

$this->append('form-start', $this->Form->create($identityProvider, [
    'class' => 'protected-form',
]));

$this->start('tab-heading');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Identity provider'), '#idp-main');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'SignIn service'), '#idp-signIn');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Logout service'), '#idp-logout');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Attributes'), '#idp-attributes');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Security'), '#idp-security');
$this->end();

$this->start('tab-content');
echo $this->Html->tabStart('idp-main');
echo $this->Form->input('name', [
    'label' => __d('cvo_technologies/saml_login', 'Name'),
    'data-slug' => '#alias',
    'data-slug-editable' => true,
    'data-slug-edit-class' => 'btn btn-secondary btn-sm',
]);
echo $this->Form->input('alias', [
    'class' => 'slug',
    'label' => __d('cvo_technologies/saml_login', 'Alias'),
]);
echo $this->Form->input('entity_id', [
    'label' => __d('cvo_technologies/saml_login', 'Entity ID'),
    'type' => 'text',
    'disabled' => $identityProvider->isDefinedInMetadata('entity_id')
]);
echo $this->Form->input('create_user', [
    'label' => __d('cvo_technologies/saml_login', 'Create users'),
]);
echo $this->Form->input('update_user', [
    'label' => __d('cvo_technologies/saml_login', 'Update users'),
]);
echo $this->Form->input('metadata', [
    'label' => __d('cvo_technologies/saml_login', 'Metadata'),
    'type' => 'textarea'
]);
echo $this->Html->tabEnd();
echo $this->Html->tabStart('idp-signIn');
echo $this->Form->input('sso_url', [
    'label' => __d('cvo_technologies/saml_login', 'SSO URL'),
    'disabled' => $identityProvider->isDefinedInMetadata('sso_url')
]);
echo $this->Form->input('sso_binding', [
    'label' => __d('cvo_technologies/saml_login', 'Binding'),
    'options' => [
        'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect' => 'HTTP-Redirect',
        'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST' => 'HTTP-POST',
        'urn:oasis:names:tc:SAML:2.0:bindings:SOAP' => 'SOAP',
    ],
    'disabled' => $identityProvider->isDefinedInMetadata('sso_binding')
]);
echo $this->Html->tabEnd();
echo $this->Html->tabStart('idp-logout');
echo $this->Form->input('slo_url', [
    'label' => __d('cvo_technologies/saml_login', 'SLO URL'),
    'disabled' => $identityProvider->isDefinedInMetadata('slo_url')
]);
echo $this->Form->input('slo_binding', [
    'label' => __d('cvo_technologies/saml_login', 'Binding'),
    'options' => [
        'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect' => 'HTTP-Redirect',
        'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST' => 'HTTP-POST',
        'urn:oasis:names:tc:SAML:2.0:bindings:SOAP' => 'SOAP',
    ],
    'disabled' => $identityProvider->isDefinedInMetadata('slo_binding')
]);
echo $this->Html->tabEnd('idp-logout');
echo $this->Html->tabStart('idp-attributes');
echo $this->Form->input('login_field', [
    'label' => __d('cvo_technologies/saml_login', 'Login field'),
    'options' => [
        'username' => __d('cvo_technologies/saml_login', 'Username'),
        'email' => __d('cvo_technologies/saml_login', 'Email'),
    ],
]);
echo $this->Form->input('login_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'Login attribute'),
]);
echo $this->Form->input('first_name_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'First name attribute'),
]);
echo $this->Form->input('last_name_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'Last name attribute'),
]);
echo $this->Form->input('full_name_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'Full name attribute'),
]);
echo $this->Form->input('email_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'Email attribute'),
]);
echo $this->Form->input('website_attribute', [
    'label' => __d('cvo_technologies/saml_login', 'Website attribute'),
]);
echo $this->Html->tabEnd('idp-attributes');
echo $this->Html->tabStart('idp-security');
echo $this->Form->input('signing_certificate', [
    'label' => __d('cvo_technologies/saml_login', 'Signing cert'),
    'disabled' => $identityProvider->isDefinedInMetadata('certData')
]);
echo $this->Html->tabEnd('idp-security');
$this->end();

$this->start('panels');
echo $this->Html->beginBox(__d('cvo_technologies/saml_login', 'Publishing'));
echo $this->element('Croogo/Core.admin/buttons', ['type' => $identityProvider->name]);
echo $this->Html->endBox();
$this->end();
