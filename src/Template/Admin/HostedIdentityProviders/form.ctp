<?php

$this->extend('Croogo/Core./Common/admin_edit');
$this->Html->script(array('Croogo/Nodes.admin'), ['block' => true]);

$this->Html
    ->addCrumb(__d('cvo_technologies/saml_login', 'SAML'))
    ->addCrumb(__d('cvo_technologies/saml_login', 'Hosted identity providers'), ['action' => 'index']);

if ($this->request->params['action'] == 'add') {
    $this->assign('title', __d('cvo_technologies/saml_login', 'Create hosted identity provider'));

    $this->Html->addCrumb(__d('cvo_technologies/saml_login', 'Create'), ['action' => 'add']);
}

if ($this->request->params['action'] == 'edit') {
    $this->assign('title', __d('cvo_technologies/saml_login', 'Edit {0}', $hostedIdentityProvider->entity_id));

    $this->Html->addCrumb($hostedIdentityProvider->entity_id);
}

$this->append('form-start', $this->Form->create($hostedIdentityProvider, [
    'class' => 'protected-form',
]));

$this->start('tab-heading');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Hosted identity provider'), '#idp-main');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Attributes'), '#idp-attributes');
echo $this->Croogo->adminTab(__d('cvo_technologies/saml_login', 'Security'), '#idp-security');
$this->end();

$this->start('tab-content');
echo $this->Html->tabStart('idp-main');
echo $this->Form->input('entity_id', [
    'label' => __d('cvo_technologies/saml_login', 'Entity ID'),
    'type' => 'text',
    'data-slug' => '#alias',
    'data-slug-editable' => true,
    'data-slug-edit-class' => 'btn btn-secondary btn-sm',
]);
echo $this->Form->input('alias', [
    'class' => 'slug',
    'label' => __d('cvo_technologies/saml_login', 'Alias'),
]);
echo $this->Form->input('host', [
    'label' => __d('cvo_technologies/saml_login', 'Host'),
]);
echo $this->Form->input('authsource_id', [
    'label' => __d('cvo_technologies/saml_login', 'Authsource'),
]);
echo $this->Html->tabEnd();
echo $this->Html->tabStart('idp-attributes');
echo $this->Form->input('name_id_format', [
    'label' => __d('cvo_technologies/saml_login', 'NameID format'),
]);
echo $this->Form->input('attribute_user_id', [
    'type' => 'text',
    'label' => __d('cvo_technologies/saml_login', 'User ID attribute'),
    'help' => __d('cvo_technologies/saml_login', 'The attribute to base persistent name ID\'s on')
]);
echo $this->Form->input('attribute_email', [
    'type' => 'text',
    'label' => __d('cvo_technologies/saml_login', 'Email attribute'),
]);
echo $this->Html->tabEnd('idp-attributes');
echo $this->Html->tabStart('idp-security');
echo $this->Form->input('certificate', [
    'label' => __d('cvo_technologies/saml_login', 'Certificate path'),
]);
echo $this->Form->input('private_key', [
    'label' => __d('cvo_technologies/saml_login', 'Private key path'),
]);
echo $this->Html->tabEnd('idp-security');
$this->end();

$this->start('panels');
echo $this->Html->beginBox(__d('cvo_technologies/saml_login', 'Publishing'));
echo $this->element('Croogo/Core.admin/buttons', ['type' => $hostedIdentityProvider->entity_id]);
echo $this->Html->endBox();
$this->end();
