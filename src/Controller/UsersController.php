<?php

namespace CvoTechnologies\SamlLogin\Controller;

use Cake\Event\Event;

class UsersController extends AppController
{
    public function login()
    {
        $this->loadModel('CvoTechnologies/SamlLogin.IdentityProviders');

        $this->set('identityProviders', $this->IdentityProviders->find());
        $this->set('returnUrl', $this->Auth->redirectUrl());
    }
}
