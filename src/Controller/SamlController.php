<?php

namespace CvoTechnologies\SamlLogin\Controller;

use CvoTechnologies\SimpleSaml\Controller\SamlAuthenticationTrait;

class SamlController extends AppController
{
    use SamlAuthenticationTrait;
}
