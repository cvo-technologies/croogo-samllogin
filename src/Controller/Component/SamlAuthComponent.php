<?php

namespace CvoTechnologies\SamlLogin\Controller\Component;

use Cake\Controller\Component;

class SamlAuthComponent extends Component
{
    public function startup()
    {
        $this->_registry->Auth->config('loginAction', [
            'plugin' => 'CvoTechnologies/SamlLogin',
            'controller' => 'Users',
            'action' => 'login'
        ]);

        $authenticate = $this->_registry->Auth->config('authenticate');
        $authenticate['CvoTechnologies/SimpleSaml.SimpleSaml'] = [
            'userModel' => 'Croogo/Users.Users'
        ];

        $this->_registry->Auth->configShallow('authenticate', $authenticate);
    }
}
