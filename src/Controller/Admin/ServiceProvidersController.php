<?php

namespace CvoTechnologies\SamlLogin\Controller\Admin;

use Cake\Core\Configure;

class ServiceProvidersController extends AppController
{
    public function info()
    {
        $this->set('settings', Configure::read('Saml'));

        $cert = openssl_x509_read(Configure::read('Saml.sp.x509cert'));

        $this->set('privateKeyMatches', openssl_x509_check_private_key($cert, Configure::read('Saml.sp.privateKey')));
        $this->set('certificateFingerprint', openssl_x509_fingerprint($cert));
        $this->set('certificateInformation', openssl_x509_parse($cert));

        openssl_x509_free($cert);
    }
}
