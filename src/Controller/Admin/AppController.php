<?php

namespace CvoTechnologies\SamlLogin\Controller\Admin;

use Cake\Event\Event;
use Croogo\Core\Controller\Admin\AppController as BaseController;
use Croogo\Core\Model\Table\CroogoTable;

class AppController extends BaseController
{
    public function initialize()
    {
        parent::initialize();

        if (!$this->{$this->name} instanceof CroogoTable) {
            return;
        }

        $this->Crud->config('actions.index', [
            'displayFields' => $this->{$this->name}->displayFields(),
            'searchFields' => ['alias', 'title']
        ]);
    }
}
