<?php

namespace CvoTechnologies\SamlLogin\Controller\Admin;

use Cake\Event\Event;

class UsersController extends AppController
{
    /**
     * {@inheritDoc}
     */
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['login']);
    }

    public function login()
    {
        $this->loadModel('CvoTechnologies/SamlLogin.IdentityProviders');

        $this->set('identityProviders', $this->IdentityProviders->find());
        $this->set('returnUrl', $this->Auth->redirectUrl());
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);

        $this->viewBuilder()
            ->layout('Croogo/Users.admin_login');
    }
}
