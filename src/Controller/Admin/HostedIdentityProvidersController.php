<?php

namespace CvoTechnologies\SamlLogin\Controller\Admin;

class HostedIdentityProvidersController extends AppController
{
    public function implementedEvents()
    {
        return parent::implementedEvents() + [
            'Crud.beforeFind' => 'beforeCrudFind'
        ];
    }

    public function beforeCrudFind()
    {
        $this->set('authsources', $this->HostedIdentityProviders->Authsources->find('list'));
    }
}
