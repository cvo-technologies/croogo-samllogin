<?php

namespace CvoTechnologies\SamlLogin\Model\Table;

use Croogo\Core\Model\Table\CroogoTable;

class ServiceProviderAssertionConsumerServicesTable extends CroogoTable
{
    /**
     * {@inheritDoc}
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('saml_service_provider_assertion_consumer_services');
    }
}
