<?php

namespace CvoTechnologies\SamlLogin\Model\Table;

use Croogo\Core\Model\Table\CroogoTable;

class IdentityProviderManageNameIdServicesTable extends CroogoTable
{
    /**
     * {@inheritDoc}
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('saml_identity_provider_manage_name_id_services');
    }
}
