<?php

namespace CvoTechnologies\SamlLogin\Model\Table;

use Croogo\Core\Model\Table\CroogoTable;

class IdentityProviderSignOnServicesTable extends CroogoTable
{
    /**
     * {@inheritDoc}
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('saml_identity_provider_sign_on_services');
    }
}
