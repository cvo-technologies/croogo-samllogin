<?php

namespace CvoTechnologies\SamlLogin\Model\Table;

use Cake\Database\Schema\Table as Schema;
use Croogo\Core\Model\Table\CroogoTable;

class AuthsourcesTable extends CroogoTable
{
    protected $_displayFields = [
        'alias',
        'entity_id' => ['label' => 'Entity ID'],
    ];

    /**
     * {@inheritDoc}
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('saml_authsources');
        $this->displayField('alias');

        $this->addBehavior('Search.Search');
        $this->addBehavior('CvoTechnologies/SamlLogin.SettingsStoring', [
            'keyPrefix' => 'SimpleSaml.authsources',
            'mapping' => [
                'entity_id' => 'entityID',
                'type' => '0',
                'certificate' => 'certData',
                'private_key' => 'privatekey',
                'name_id_policy' => 'NameIDPolicy'
            ]
        ]);

        $this->hasMany('HostedIdentityProviders', [
            'className' => 'CvoTechnologies/SamlLogin.HostedIdentityProviders'
        ]);
    }

    protected function _initializeSchema(Schema $table)
    {
        $table->columnType('certificate', 'certificate');

        return parent::_initializeSchema($table);
    }
}
