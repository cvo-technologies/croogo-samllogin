<?php
namespace CvoTechnologies\SamlLogin\Model\Table;

use Cake\Database\Schema\Table as Schema;
use Cake\Validation\Validator;
use Croogo\Core\Model\Table\CroogoTable;

class HostedIdentityProvidersTable extends CroogoTable
{
    protected $_displayFields = [
        'entity_id' => ['label' => 'Entity ID'],
        'host',
    ];

    /**
     * {@inheritDoc}
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('saml_hosted_identity_providers');
        $this->displayField('entity_id');

        $this->addBehavior('Search.Search');
        $this->addBehavior('CvoTechnologies/SamlLogin.SettingsStoring', [
            'keyPrefix' => 'SimpleSaml.saml20-idp-hosted',
            'mapping' => [
                'entity_id' => 'entityID',
                'host' => 'host',
                'certificate' => 'certificate',
                'private_key' => 'privatekey',
                'Authsources.alias' => 'auth',
                'name_id_format' => 'NameIDFormat',
                'attribute_user_id' => 'userid.attribute',
                'attribute_email' => 'attributes.email',
            ]
        ]);

        $this->belongsTo('Authsources', [
            'className' => 'CvoTechnologies/SamlLogin.Authsources'
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('alias', 'create')
            ->notEmpty('alias');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');
        $validator
            ->add('signing_cert', 'validSigningCertificate', [
                'rule' => function ($value) {
                    return (bool)@openssl_x509_read($value);
                }
            ])
            ->allowEmpty('signing_cert');

        return $validator;
    }
}
