<?php

namespace CvoTechnologies\SamlLogin\Model\Table;

use Croogo\Core\Model\Table\CroogoTable;

class ServiceProviderLogoutServicesTable extends CroogoTable
{
    /**
     * {@inheritDoc}
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('saml_service_provider_logout_services');
    }
}
