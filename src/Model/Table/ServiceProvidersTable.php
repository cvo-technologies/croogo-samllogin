<?php

namespace CvoTechnologies\SamlLogin\Model\Table;

use Cake\Database\Schema\Table as Schema;
use Cake\Validation\Validator;
use Croogo\Core\Model\Table\CroogoTable;

class ServiceProvidersTable extends CroogoTable
{
    protected $_displayFields = [
        'name',
        'entity_id' => ['label' => 'Entity ID'],
        'create_user' => ['type' => 'boolean'],
        'update_user' => ['type' => 'boolean'],
    ];

    /**
     * {@inheritDoc}
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('saml_service_providers');
        $this->displayField('title');

        $this->addBehavior('Search.Search');
        $this->addBehavior('CvoTechnologies/SamlLogin.SettingsStoring', [
            'keyPrefix' => 'SimpleSaml.saml20-sp-remote',
            'mapping' => [
                'entity_id' => 'entityID',
                'name' => 'name',
                'description' => 'description',
                'certificate' => 'certData',
                'private_key' => 'privatekey',
                'name_id_format' => 'NameIDFormat'
            ],
            'associationMapping' => [
                'LogoutServices' => [
                    'attribute' => 'SingleLogoutService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location'
                    ]
                ],
                'AssertionConsumerServices' => [
                    'attribute' => 'AssertionConsumerService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                        'index' => 'index',
                    ]
                ]
            ]
        ]);
        $this->addBehavior('CvoTechnologies/SamlLogin.Metadata', [
            'mapping' => [
                'entity_id' => 'entityId',
                'certificate' => 'certData',
                'name_id_format' => 'NameIDFormat',
            ],
            'associationMapping' => [
                'LogoutServices' => [
                    'attribute' => 'SingleLogoutService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location'
                    ]
                ],
                'AssertionConsumerServices' => [
                    'attribute' => 'AssertionConsumerService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                        'index' => 'index',
                    ]
                ]
            ]
        ]);

        $this->hasMany('LogoutServices', [
            'className' => 'CvoTechnologies/SamlLogin.ServiceProviderLogoutServices'
        ]);
        $this->hasMany('AssertionConsumerServices', [
            'className' => 'CvoTechnologies/SamlLogin.ServiceProviderAssertionConsumerServices'
        ]);
    }

    /**
     * {@inheritDoc}
     */
    protected function _initializeSchema(Schema $table)
    {
        $table->columnType('metadata', 'saml_metadata');
        $table->columnType('certificate', 'certificate');

        return parent::_initializeSchema($table);
    }

    /**
     * {@inheritDoc}
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('alias', 'create')
            ->notEmpty('alias');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');
        $validator
            ->add('signing_cert', 'validSigningCertificate', [
                'rule' => function ($value) {
                    return (bool)@openssl_x509_read($value);
                }
            ])
            ->allowEmpty('signing_cert');

        return $validator;
    }
}
