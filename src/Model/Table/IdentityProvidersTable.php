<?php

namespace CvoTechnologies\SamlLogin\Model\Table;

use Cake\Database\Schema\Table as Schema;
use Croogo\Core\Model\Table\CroogoTable;

class IdentityProvidersTable extends CroogoTable
{
    protected $_displayFields = [
        'name',
        'entity_id' => ['label' => 'Entity ID'],
        'create_user' => ['type' => 'boolean'],
        'update_user' => ['type' => 'boolean'],
    ];

    /**
     * {@inheritDoc}
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('saml_identity_providers');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Search.Search');
        $this->addBehavior('CvoTechnologies/SamlLogin.SettingsStoring', [
            'keyPrefix' => 'SimpleSaml.saml20-idp-remote',
            'mapping' => [
                'entity_id' => 'entityID',
                'name' => 'name',
                'signing_certificate' => 'certData',
                'name_id_format' => 'NameIDFormat',
                'login_field' => 'loginField',
                'login_attribute' => 'attributes.login',
                'first_name_attribute' => 'attributes.firstName',
                'last_name_attribute' => 'attributes.lastName',
                'full_name_attribute' => 'attributes.fullName',
                'email_attribute' => 'attributes.email',
                'website_attribute' => 'attributes.website',
            ],
            'associationMapping' => [
                'LogoutServices' => [
                    'attribute' => 'SingleLogoutService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                        'response_location' => 'ResponseLocation',
                    ]
                ],
                'SignOnServices' => [
                    'attribute' => 'SingleSignOnService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                    ]
                ],
                'ArtifactResolutionServices' => [
                    'attribute' => 'ArtifactResolutionService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                        'service_index' => 'index',
                        'is_default' => 'isDefault'
                    ]
                ],
                'ManageNameIdServices' => [
                    'attribute' => 'ManageNameIDService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                        'response_location' => 'ResponseLocation',
                    ]
                ],
                'NameIdMappingServices' => [
                    'attribute' => 'NameIDMappingService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                    ]
                ]
            ]
        ]);
        $this->addBehavior('CvoTechnologies/SamlLogin.Metadata', [
            'mapping' => [
                'entity_id' => 'entityID',
                'signing_certificate' => 'certData',
                'name_id_format' => 'NameIDFormat',
            ],
            'associationMapping' => [
                'LogoutServices' => [
                    'attribute' => 'SingleLogoutService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                        'response_location' => 'ResponseLocation',
                    ]
                ],
                'SignOnServices' => [
                    'attribute' => 'SingleSignOnService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                    ]
                ],
                'ArtifactResolutionServices' => [
                    'attribute' => 'ArtifactResolutionService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                        'service_index' => 'index',
                        'is_default' => 'isDefault'
                    ]
                ],
                'ManageNameIdServices' => [
                    'attribute' => 'ManageNameIDService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                        'response_location' => 'ResponseLocation',
                    ]
                ],
                'NameIdMappingServices' => [
                    'attribute' => 'NameIDMappingService',
                    'mapping' => [
                        'binding' => 'Binding',
                        'location' => 'Location',
                    ]
                ]
            ]
        ]);

        $this->hasMany('LogoutServices', [
            'className' => 'CvoTechnologies/SamlLogin.IdentityProviderLogoutServices'
        ]);
        $this->hasMany('SignOnServices', [
            'className' => 'CvoTechnologies/SamlLogin.IdentityProviderSignOnServices'
        ]);
        $this->hasMany('ArtifactResolutionServices', [
            'className' => 'CvoTechnologies/SamlLogin.IdentityProviderArtifactResolutionServices'
        ]);
        $this->hasMany('ManageNameIdServices', [
            'className' => 'CvoTechnologies/SamlLogin.IdentityProviderManageNameIdServices'
        ]);
        $this->hasMany('NameIdMappingServices', [
            'className' => 'CvoTechnologies/SamlLogin.IdentityProviderNameIdMappingServices'
        ]);
    }

    /**
     * {@inheritDoc}
     */
    protected function _initializeSchema(Schema $table)
    {
        $table->columnType('metadata', 'saml_metadata');
        $table->columnType('signing_certificate', 'certificate');

        return parent::_initializeSchema($table);
    }
}
