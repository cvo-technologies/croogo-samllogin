<?php

namespace CvoTechnologies\SamlLogin\Model\Table;

use Croogo\Core\Model\Table\CroogoTable;

class IdentityProviderNameIdMappingServicesTable extends CroogoTable
{
    /**
     * {@inheritDoc}
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('saml_identity_provider_name_id_mapping_services');
    }
}
