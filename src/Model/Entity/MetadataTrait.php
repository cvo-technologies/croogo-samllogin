<?php

namespace CvoTechnologies\SamlLogin\Model\Entity;

trait MetadataTrait
{
    public function isDefinedInMetadata($property)
    {
        if (!$this->metadata) {
            return false;
        }

        return (bool)$this->metadata->config(static::PROPERTY_MAPPING[$property]);
    }
}
