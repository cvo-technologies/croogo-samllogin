<?php

namespace CvoTechnologies\SamlLogin\Model\Entity;

use Cake\ORM\Entity;

class IdentityProvider extends Entity
{
    use MetadataTrait;

    const PROPERTY_MAPPING = [
    ];

    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
