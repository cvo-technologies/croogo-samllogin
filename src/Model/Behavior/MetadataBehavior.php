<?php

namespace CvoTechnologies\SamlLogin\Model\Behavior;

use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;

class MetadataBehavior extends Behavior
{
    protected $_defaultConfig = [
        'mapping' => []
    ];

    public function beforeSave(Event $event, Entity $entity)
    {
        if (!$entity->metadata) {
            return;
        }

        $patchData = $entity->toArray();
        foreach ($this->config('mapping') as $property => $config) {
            if (!$entity->metadata->config($config)) {
                continue;
            }

            $patchData[$property] = $entity->metadata->config($config);
        }
        foreach ($this->config('associationMapping') as $associationName => $settings) {
            if (!$entity->metadata->config($settings['attribute'])) {
                continue;
            }

            $association = $this->_table->association($associationName);
            foreach ((array)$entity->metadata->config($settings['attribute']) as $index => $associationValues) {
                foreach ($settings['mapping'] as $property => $config) {
                    if (!isset($associationValues[$config])) {
                        continue;
                    }

                    $patchData{$association->property()}[$index][$property] = $associationValues[$config];
                }
            }
        }

        $this->_table->patchEntity($entity, $patchData, [
            'associated' => array_keys($this->config('associationMapping'))
        ]);
    }
}
