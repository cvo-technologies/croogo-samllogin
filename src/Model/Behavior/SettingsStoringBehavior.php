<?php

namespace CvoTechnologies\SamlLogin\Model\Behavior;

use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use CvoTechnologies\SimpleSaml\Certificate;

class SettingsStoringBehavior extends Behavior
{
    protected $_defaultConfig = [
        'keyPrefix' => 'SimpleSaml',
        'keyField' => 'alias',
        'mapping' => [],
        'associationMapping' => []
    ];

    public function afterSave(Event $event, Entity $entity)
    {
        $settings = TableRegistry::get('Croogo/Settings.Settings');

        $keyPrefix = $this->config('keyPrefix');
        $keyField = $this->config('keyField');

        if ($entity->dirty('alias')) {
            $originalKeyField = $entity->getOriginal($keyField);
            foreach ($this->config('mapping') as $property => $config) {
                if (is_array($entity->get($property))) {
                    foreach (array_keys($entity->get($property)) as $index) {
                        $settings->deleteKey($keyPrefix . '.' . $originalKeyField . '.' . $config . '.' . $index);
                    }

                    continue;
                }

                $settings->deleteKey($keyPrefix . '.' . $originalKeyField . '.' . $config);
            }
            foreach ($this->config('associationMapping') as $associationName => $associationSettings) {
                $association = $this->_table->association($associationName);
                foreach ((array)$entity->get($association->property()) as $index => $associationValue) {
                    foreach ($associationSettings['mapping'] as $field => $attribute) {
                        $key = $keyPrefix . '.' . $originalKeyField . '.' . $associationSettings['attribute'] . '.' . $index . '.' . $attribute;

                        $settings->deleteKey($key);
                    }
                }
            }
        }

        $settingsList = [];
        foreach ($this->config('mapping') as $property => $config) {
            $value = $entity->get($property);

            if (strstr($property, '.')) {
                list($associationName, $field) = explode('.', $property);
                if (!$this->_table->association($associationName)) {
                    continue;
                }
                $associationProperty = $this->_table->association($associationName)->property();

                $loadedEntity = $this->_table->loadInto($entity, [
                    $associationName
                ]);
                $value = $loadedEntity->get($associationProperty)->get($field);
            }

            $settingsList[$config] = $value;
        }
        foreach ($this->config('associationMapping') as $associationName => $associationSettings) {
            $association = $this->_table->association($associationName);
            foreach ((array)$entity->get($association->property()) as $index => $associationValue) {
                foreach ($associationSettings['mapping'] as $field => $attribute) {
                    $value = $associationValue[$field];

                    $settingsList[$associationSettings['attribute']][$index][$attribute] = $value;
                }
            }
        }

        $settingsList = Hash::flatten($settingsList);

        foreach ($settingsList as $config => $value) {
            $key = $keyPrefix . '.' . $entity->get($keyField) . '.' . $config;
            if ($value instanceof Certificate) {
                $value = $value->certificateData();
            }

            $settings->write($key, $value);
        }
    }

    public function afterDelete(Event $event, Entity $entity)
    {
        $settings = TableRegistry::get('Croogo/Settings.Settings');
        foreach ($this->config('mapping') as $property => $config) {
            $settings->deleteKey($this->config('keyPrefix') . '.' . $entity->get($this->config('keyField')) . '.' . $config);
        }
        foreach ($this->config('associationMapping') as $associationName => $associationSettings) {
            $association = $this->_table->association($associationName);
            foreach ((array)$entity->get($association->property()) as $index => $associationValue) {
                foreach ($associationSettings['mapping'] as $field => $attribute) {
                    $key = $this->config('keyPrefix') . '.' . $entity->get($this->config('keyField')) . '.' . $associationSettings['attribute'] . '.' . $index . '.' . $attribute;

                    $settings->deleteKey($key);
                }
            }
        }
    }
}
