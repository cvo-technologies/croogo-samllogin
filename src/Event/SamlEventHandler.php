<?php

namespace CvoTechnologies\SamlLogin\Event;

use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;

class SamlEventHandler implements EventListenerInterface
{
    public function implementedEvents()
    {
        return [
            'Saml.userFound' => [
                'callable' => 'onUserFound',
            ],
        ];
    }

    public function onUserFound(Event $event)
    {
        if ((!isset($event->data()['config']['updateUser'])) || (!$event->data()['config']['updateUser'])) {
            return;
        }

        if ($event->data()['attributes']['fullName']) {
            $event->data()['user']->name = $event->data()['attributes']['fullName'][0];
        }
        if (($event->data()['attributes']['firstName']) && ($event->data()['attributes']['lastName'])) {
            $event->data()['user']->name = $event->data()['attributes']['firstName'][0] . ' ' . $event->data()['attributes']['lastName'][0];
        }
        if ($event->data()['attributes']['website']) {
            $event->data()['user']->website = $event->data()['attributes']['website'][0];
        }

        TableRegistry::get('Croogo/Users.Users')->save($event->data()['user']);
    }
}
